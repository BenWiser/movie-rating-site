import MovieDAO from '../movie/dao';
import Movie from '../movie/model';

import Rating_DatabaseModel from './databaseModel';

class RatingDAO {
    public async rateMovies(first: Partial<Movie>, rating: -1 | 0 | 1, second: Partial<Movie>) {
        await Promise.all([
            Rating_DatabaseModel.create({
                firstMovie: first.index,
                secondMovie: second.index,
                relationship: rating,
            }),
            MovieDAO.rateMovie(first, rating),
            MovieDAO.rateMovie(second, rating * -1),
        ]);
    }
}

export default new RatingDAO();
