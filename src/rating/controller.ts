import { Response, Render, BodyParams, PathParams, Redirect, Get, Post, Controller } from '@tsed/common';

import Rating from './model';
import MovieDAO from '../movie/dao';
import RatingDAO from './dao';

@Controller('/movies/rating')
export default class RatingController {

    /**
     *  This is the route you go to to review a movie
     */
    @Get('/')
    public index() {

    }

    @Get('/:index')
    @Render('rating/create')
    public async rateMovie(@PathParams('index') index: number) {
        const movie = await MovieDAO.getMovie(index);
        const moviesToCompare = await MovieDAO.getNMoviesWithoutMovie(1, movie);

        return {
            movie,
            moviesToCompare,
        };
    }

    @Post('/:index')
    public async saveMovieRatings(@PathParams('index') index: number, @BodyParams('ratings') ratings: Array<Rating>, @Response() response) {
        for (const rating of ratings) {
            // TODO: Update this to use the actual objects
            const first = await MovieDAO.getMovie(Number(rating.first as any));
            const second = await MovieDAO.getMovie(Number(rating.second as any));

            await RatingDAO.rateMovies(first, Number(rating.relationship) as -1 | 0 | 1, second);
        }

        response.redirect(`/movies/${index}`);
    }
}
