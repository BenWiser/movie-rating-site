import { Model, Table, HasOne, Column, DataType } from 'sequelize-typescript';
import Movie from '../movie/databaseModel';

@Table({
    tableName: 'ratings',
})
export default class Rating extends Model<Rating> {
    @Column({
        primaryKey: true,
    })
    public id: number;

    @HasOne(() => Movie, 'firstMovie')
    public first: Movie;

    @HasOne(() => Movie, 'secondMovie')
    public second: Movie;

    @Column({
        type: DataType.NUMBER
    })
    public relationship: number;
}