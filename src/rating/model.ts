import Movie from '../movie/model';

export default class Rating {
    static WORSE: number = -1;
    static BETTER: number = 1;
    static SAME: number = 0;

    constructor(
        public readonly first: Partial<Movie>,
        public readonly relationship: -1 | 0 | 1,
        public readonly second: Partial<Movie>,
    ) {}

}

