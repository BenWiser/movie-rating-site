const REQUIRED_RATINGS: number = 1;

export default abstract class RatingModel {
    constructor(
        public rating: number = 0,
        public reviewedEnough: boolean = false,
        public goodRatings: number = 0,
        public badRatings: number = 0
    ) {}

    public incrementGoodRatings() {
        this.goodRatings += 1;
        this._calculateRating();
        this._calculateReviewedEnough();
    }

    public incrementBadRatings() {
        this.badRatings += 1;
        this._calculateRating();
        this._calculateReviewedEnough();
    }

    // TODO: Investigate alternative ranking systems (maybe elo?)
    private _calculateRating(): void {
        this.rating = (this.goodRatings / (this.goodRatings + this.badRatings)) * 100;
    }

    private _calculateReviewedEnough(): void {
        this.reviewedEnough = this.goodRatings + this.badRatings >= REQUIRED_RATINGS;
    }
}
