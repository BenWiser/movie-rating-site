import { Render, Controller, Get } from '@tsed/common';
import MovieDAO from '../movie/dao';

@Controller('/')
export default class HomeController {
    @Get('/')
    @Render('home')
    async index() {
        return {
            movies: await MovieDAO.getMovies(),
        };
    }
}
