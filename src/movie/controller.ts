import { Get, PathParams, Post, Controller, Render, BodyParams, Redirect } from '@tsed/common';
import MovieDAO from '../movie/dao';
import Movie from '../movie/model';

@Controller('/movies')
export default class MovieController {
    /**
     * The route to add a movie
     */
    @Get('/add')
    @Render('movie/create')
    public index() {
    }

    /**
     *  The route to view a movie
     */
    @Get('/:index')
    @Render('movie/view')
    public async view(@PathParams('index') index: number) {
        const movie = await MovieDAO.getMovie(index);
        return {
            movie,
        };
    }

    /**
     *  Add a movie to the catalogue
     */
    @Post('/add')
    @Redirect('/')
    public async addMovie(@BodyParams('movie') movie: Movie) {
        await MovieDAO.addMovie(movie);
    }

}
