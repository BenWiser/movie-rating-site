import Movie from './model';
import Movie_DatabaseModel from './databaseModel';
import { Op, Sequelize } from 'sequelize';

class MovieDAO {
    private mapMovie = (movie) => new Movie(
        movie.id,
        movie.title,
        movie.year,
        movie.description,
        movie.rating,
        movie.reviewedEnough,
        movie.goodRatings,
        movie.badRatings,
    );

    public addMovie = async (movie: Partial<Movie>) => {
        await Movie_DatabaseModel.create(movie);
    };

    public getMovies = async () => {
        const movies = Movie_DatabaseModel.findAll();
        return movies.map(this.mapMovie);
    };

    public getMovie = async (index: number) => {
        const movie = await Movie_DatabaseModel.findOne({
            where: {
                id: index,
            },
        });
        if (movie === null) {
            return null;
        }
        return this.mapMovie(movie);
    };

    /**
     *  TODO: Make the _movies returned random but based off a user's id
     */
    public getNMoviesWithoutMovie = async (nMovies: number, withoutMovie: Partial<Movie>): Promise<Array<Partial<Movie>>> => {
        const movies = await Movie_DatabaseModel.findAll({
            limit: nMovies,
            order: Sequelize.literal(`RANDOM()`),
            where: {
                id: {
                    [Op.ne]: [withoutMovie.index],
                },
            },
        });

        return movies.map(this.mapMovie);
    };

    public removeMovie = async (movie: Movie) => {
        await Movie_DatabaseModel.destroy({
            where: {
                id: movie.index,
            },
        });
    };

    public rateMovie = async (movie: Partial<Movie>, rating: number) => {
        switch (rating) {
            case -1:
                movie.incrementBadRatings();
                break;
            case 1:
                movie.incrementGoodRatings();
                break;
        }
        await Movie_DatabaseModel.update(movie, {
            where: {
                id: movie.index,
            },
        });
    };
}

export default new MovieDAO();
