import RatingModel from "../rating/ratingModel/model";

export default class Movie extends RatingModel {
    constructor (
        public readonly index: number,
        public readonly title?: string,
        public readonly year?: Date,
        public readonly description?: string,
        rating: number = 0,
        reviewedEnough: boolean = false,
        goodRatings: number = 0,
        badRatings: number = 0
    ) {
        super(
            rating,
            reviewedEnough,
            goodRatings,
            badRatings,
        );
    }
}
