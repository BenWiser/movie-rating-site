import { Model, Table, Column, DataType } from 'sequelize-typescript';

@Table({
    tableName: 'movies',
})
export default class Movie extends Model<Movie> {
    @Column({
        primaryKey: true,
    })
    public id: number;

    @Column({
        type: DataType.STRING,
    })
    public title: string;

    @Column({
        type: DataType.DATE,
    })
    public year: Date;

    @Column({
        type: DataType.STRING,
    })
    public description: string;

    @Column({
        type: DataType.NUMBER,
    })
    public rating: number;

    @Column({
        type: DataType.BOOLEAN,
    })
    public reviewedEnough: boolean;

    @Column({
        type: DataType.NUMBER,
    })
    public goodRatings: number;

    @Column({
        type: DataType.NUMBER,
    })
    public badRatings: number;
}