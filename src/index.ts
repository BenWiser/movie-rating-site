import {ServerLoader, ServerSettings, GlobalAcceptMimesMiddleware} from "@tsed/common";
import handlebars from './utilities/handlebars';
import databaseConnection from './utilities/databaseConnection';
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const rootDir = __dirname;
const Path = require('path');

@ServerSettings({
    rootDir,
    acceptMimes: ["application/json", "application/xhtml+xml", "text/html"],
    mount: {
        '/': Path.resolve(__dirname, '**', 'controller.ts'),
    },
    port: 8080,
})
export class Server extends ServerLoader {
  public $onMountingMiddlewares(): void|Promise<any> {
        this
            .engine('handlebars', handlebars)
            .use(GlobalAcceptMimesMiddleware)
            .use(cookieParser())
            .use(compress({}))
            .use(express.static('public'))
            .use(methodOverride())
            .use(bodyParser.json())
            .use(bodyParser.urlencoded({
              extended: true
            }))
            .set('view engine', 'handlebars');
  }
}

const main = async () => {
  await databaseConnection.sync({
    force: false,
  });
  new Server().start();
};

main();