import { Sequelize } from 'sequelize-typescript';
import * as Path from 'path';

export default new Sequelize({
    database: 'movie_site',
    dialect: 'sqlite',
    storage: 'development.db',
    modelPaths: [Path.resolve(__dirname, '..', '**', 'databaseModel.ts')],
});