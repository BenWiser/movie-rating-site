import * as fs from 'fs';
import * as Path from 'path';
import * as handlebars from 'express-handlebars';
import hb from 'handlebars';

const createComponent = (file) => {
    const fileData = fs.readFileSync(Path.resolve(__dirname, '..', '..', 'views', 'components', `${file}.handlebars`));
    const component = hb.compile(fileData.toString('utf8'));
    return ({ hash }) => {
        return component(hash);
    };
};

const hbs = handlebars.create({
    helpers: {
        asPercentage: (value) => `${Number(value).toFixed(2)}%`,
        ratingBlock: createComponent('RatingBlock'),
    },
});

export default hbs.engine;
