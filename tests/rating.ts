import { describe, it } from 'mocha';
import { expect } from 'chai';

import Movie from '../src/movie/model';

describe('The rating system', () => {
    it('should return the correct ratings', () => {
        const movie = new Movie(1, 'Test Movie');

        movie.incrementGoodRatings();
        expect(movie.rating).to.eql(100);

        movie.incrementBadRatings();
        expect(movie.rating).to.eql(50);

        movie.incrementGoodRatings();
        expect(~~(movie.rating)).to.eql(66);

        movie.incrementGoodRatings();
        expect(~~(movie.rating)).to.eql(75);

        movie.incrementBadRatings();
        expect(~~(movie.rating)).to.eql(60);
    });
});