# Movie Rating Site

# Installation
- Make sure you have `nvm` and `yarn` installed
- Run `yarn` to install dependencies
- Run `yarn start` to run the server

# TODOS
- Add authentication
- Add javascript for reviewing
- Add option to add while reviewing
- Add search
- Add image uploading
- Add `.env`